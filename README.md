# [paisaje sonoro]

## Description

This is a game (or visual-musical animation) prototype written in Racket
to explore the connections between visual and more o less
randomized musical events.

## Requirements

- `Racket` (<https://download.racket-lang.org/>)
- `RSound` (Racket package)

  See <https://docs.racket-lang.org/pkg/getting-started.html>
  for more information about installing Racket packages.

## Limitations

`RSound` installation under Windows may fail due to the way
the `portaudio` library is installed on this OS. 

## Usage

1. Run DrRacket 
2. Open `paisaje-sonoro.rkt`
3. Click on Run

Look into the function `handle-key` to try differents ways
of interacting with the animation via the keyboard.

